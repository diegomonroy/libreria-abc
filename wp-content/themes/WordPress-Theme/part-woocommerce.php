<?php
$banner = '';
$title = '';
$left = '';
$column = '';
if ( ! is_product() ) {
	$column = 'medium-9';
}
/* Libros */
$category_1_id = 76;
$category_1 = 'libros';
if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
	if ( ! is_product() ) {
		$banner = 'banner_categorias';
		$title = '<span>LIBROS</span>';
		$left = 'left';
		$column = 'medium-9';
	}
}
/* Café */
$category_1_id = 72;
$category_1 = 'cafe';
if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
	if ( ! is_product() ) {
		$banner = 'banner_categorias';
		$title = '<span>CAFÉ</span>';
		$left = '';
		$column = 'medium-12';
	}
}
/* Artesanías */
$category_1_id = 73;
$category_1 = 'artesanias';
if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
	if ( ! is_product() ) {
		$banner = 'banner_categorias';
		$title = '<span>ARTESANÍAS</span>';
		$left = '';
		$column = 'medium-12';
	}
}
/* Discos LPS */
$category_1_id = 36;
$category_1 = 'discos-lps';
if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
	if ( ! is_product() ) {
		$banner = 'banner_categorias';
		$title = '<span>DISCOS</span> LPS';
		$left = '';
		$column = 'medium-12';
	}
}
/* Juguetes y antigüedades */
$category_1_id = 37;
$category_1 = 'juguetes-y-antiguedades';
if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
	if ( ! is_product() ) {
		$banner = 'banner_categorias';
		$title = '<span>JUGUETES</span> Y ANTIGÜEDADES';
		$left = '';
		$column = 'medium-12';
	}
}
/* Accesorios */
$category_1_id = 77;
$category_1 = 'accesorios';
if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
	if ( ! is_product() ) {
		$banner = 'banner_categorias';
		$title = '<span>ACCESORIOS</span>';
		$left = '';
		$column = 'medium-12';
	}
}
?>
<!-- Begin Banner -->
	<section class="banner" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php dynamic_sidebar( $banner ); ?>
			</div>
		</div>
	</section>
<!-- End Banner -->
<!-- Begin Content -->
	<section class="content" data-wow-delay="0.5s">
		<div class="row breadcrumb">
			<div class="small-12 medium-6 columns">
				<p class="text-left"><span>INICIO</span> / <a href="<?php echo get_site_url(); ?>/categorias/">CATEGORÍAS</a></p>
			</div>
			<div class="small-12 medium-6 columns">
				<p class="text-right"><?php echo $title; ?></p>
			</div>
		</div>
		<div class="row">
			<?php if ( ! is_product() ) : ?>
			<div class="small-12 medium-3 columns">
				<?php dynamic_sidebar( $left ); ?>
			</div>
			<?php endif; ?>
			<div class="small-12 <?php echo $column; ?> columns">
				<div class="woocommerce">
					<?php woocommerce_content(); ?>
				</div>
			</div>
		</div>
	</section>
<!-- End Content -->