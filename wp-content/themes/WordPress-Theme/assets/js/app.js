// JavaScript Document

/* ************************************************************************************************************************

WordPress Theme

File:			app.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2018

************************************************************************************************************************ */

/* WOW */

new WOW().init();

/* jQuery */

jQuery.noConflict();

/* Foundation */

jQuery(document).ready(function ( $ ) {
	jQuery(document).foundation();
});

jQuery(document).ready(function () {
	/* Menu */
	jQuery( 'ul.sub-menu' ).addClass( 'menu' );
	/* Newsletter */
	jQuery( '.tnp-email' ).attr( 'placeholder', 'Correo electrónico' );
	jQuery( 'input.tnp-submit' ).prop( 'value', 'ENVIAR' );
});